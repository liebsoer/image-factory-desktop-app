package de.liebich.data.image.factory.app;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;

public class TestImageFactoryControler extends StackPane implements Initializable{
	
	@FXML
	private Label lblErrorMessage;
	@FXML
	private TextField tfHeight;
	@FXML
	private TextField tfWidth;
	@FXML
	private CheckBox cbText;
	@FXML
	private TextField tfText;
	@FXML
	private TextField tfFileName;
	@FXML
	private CheckBox cbFileName;
	@FXML
	private ChoiceBox<String> ddFileName;
	@FXML
	private TextField tfSavePath;
	@FXML
	private Button btnPathChooser;
	@FXML
	private ColorPicker cpBackgroundColor;
	@FXML
	private ColorPicker cpTextColor;
	@FXML
	private Button btnCreateImage;
	@FXML
	private Button btnClose;
	@FXML
	private Button btnClear;
	
	private final String DEFAULT_SAVE_PATH;
	private final String DEFAULT_FILE_NAME;
	private final String DEFAULT_FILE_EXT;
	private final String DEFAULT_TEXT;
	private final String DEFAULT_IMAGE_WIDTH;
	private final String DEFAULT_IMAGE_HEIGHT;
	private final String DEFAULT_BACKGROUND_COLOR;
	private final String DEFAULT_TEXT_COLOR;
	private final List<String> SUPPORTED_IMAGE_TYPES;
	private final Properties DEFAULT_PROPERTIES;
	private final static String PROPERTY_NAMESPACE = "config.default";
	private final static Map<String, String> DEFAULT_VALUES = new HashMap<String, String>();
	{
		final String home = System.getProperty("user.home") + 
				(System.getProperty("user.home").charAt(System.getProperty("user.home").length() - 1) != File.separatorChar 
				 ? File.separator : "");
		DEFAULT_VALUES.put("home", home);
		
		final String picName;
		if(new File(home + "Bilder").exists()){
			picName = "Bilder";
		} else if(new File(home + "Eigene Bilder").exists()){
			picName = "Bilder";
		} else if(new File(home + "Pictures").exists()){
			picName = "Pictures";
		} else{
			picName = "";
		}
		DEFAULT_VALUES.put("pictures", picName);
		DEFAULT_VALUES.put("width", "500");
		DEFAULT_VALUES.put("height", "300");
		DEFAULT_VALUES.put("savedirpath", "#{home}/#{pictures}");
		DEFAULT_VALUES.put("filename","#{width}x#{height}px");
		DEFAULT_VALUES.put("fileextention","png");
		DEFAULT_VALUES.put("text","#{width}x#{height}px");
		DEFAULT_VALUES.put("backgroundcolor","#000000");
		DEFAULT_VALUES.put("textcolor","#FFFFFF");
		DEFAULT_VALUES.put("extentions","png,jpg,gif");
	}
	
	private Main app;

	public TestImageFactoryControler(){
	        DEFAULT_PROPERTIES = PropertiesUtils.getProperties("config.properties");
	        
        	DEFAULT_SAVE_PATH = getProperty(PROPERTY_NAMESPACE, "savedirpath");
        	DEFAULT_FILE_NAME = getProperty(PROPERTY_NAMESPACE, "filename");
        	DEFAULT_FILE_EXT = getProperty(PROPERTY_NAMESPACE, "fileextention");
        	DEFAULT_TEXT = getProperty(PROPERTY_NAMESPACE, "text");
        	DEFAULT_IMAGE_WIDTH = getProperty(PROPERTY_NAMESPACE, "width");
        	DEFAULT_IMAGE_HEIGHT = getProperty(PROPERTY_NAMESPACE, "height");
        	DEFAULT_BACKGROUND_COLOR = getProperty(PROPERTY_NAMESPACE, "backgroundcolor");
        	DEFAULT_TEXT_COLOR = getProperty(PROPERTY_NAMESPACE, "textcolor");
        	SUPPORTED_IMAGE_TYPES = new ArrayList<String>(Arrays.asList(getProperty(PROPERTY_NAMESPACE, "extentions").split(",")));
	}
	
	private String getProperty(final String namespace, final String key){
		return replacePlaceholders(DEFAULT_PROPERTIES.getProperty(namespace + "." + key, DEFAULT_VALUES.containsKey(key) ? DEFAULT_VALUES.get(key) : ""));
	}
	
	@SuppressWarnings("unused")
    private String getProperty(final String key){
		return replacePlaceholders(DEFAULT_PROPERTIES.getProperty(key, DEFAULT_VALUES.containsKey(key) ? DEFAULT_VALUES.get(key) : ""));
	}
	
	private String replacePlaceholders(String text){
		final Iterator<Entry<String, String>> set = DEFAULT_VALUES.entrySet().iterator();
		while(set.hasNext()){
			final Entry<String, String> entry = set.next();
			final String pattern = "#{" + entry.getKey() + "}";
			while(text.contains(pattern)){
				text = text.replace(pattern, entry.getValue());
			}
			
		}
		return text;
	}
	
	public void initialize(URL location, ResourceBundle resources) {
		
		final EventHandler<KeyEvent> onlyDigitsEventHandler = new EventHandler<KeyEvent>() {
		    public void handle(KeyEvent event) {
		        if(!(event.getCharacter().charAt(0) == 8 || event.getCharacter().charAt(0) >= '0' && event.getCharacter().charAt(0) <= '9' )){
		            event.consume();                  
		        }
		    }
	    };
		final EventHandler<KeyEvent> updateFileNameEventHandler = new EventHandler<KeyEvent>(){
			public void handle(KeyEvent event) {
				final String text = tfWidth.getText() + "x" + tfHeight.getText() + "px";
				if(!cbFileName.isSelected()){
					tfFileName.setText(text);
				}
				if(!cbText.isSelected()){
					tfText.setText(text);
				}
			}
		};
	    
		this.tfHeight.setText(DEFAULT_IMAGE_HEIGHT);
		this.tfHeight.addEventFilter(KeyEvent.KEY_TYPED, onlyDigitsEventHandler); 
		this.tfHeight.addEventFilter(KeyEvent.ANY, updateFileNameEventHandler); 
		this.tfWidth.setText(DEFAULT_IMAGE_WIDTH);
		this.tfWidth.addEventFilter(KeyEvent.KEY_TYPED,  onlyDigitsEventHandler);
		this.tfWidth.addEventFilter(KeyEvent.ANY,  updateFileNameEventHandler);
		this.tfFileName.setText(DEFAULT_FILE_NAME);
		this.tfText.setText(DEFAULT_TEXT);
		this.cpBackgroundColor.setValue(Color.valueOf(DEFAULT_BACKGROUND_COLOR));
		this.cpTextColor.setValue(Color.valueOf(DEFAULT_TEXT_COLOR));
		this.tfSavePath.setText(DEFAULT_SAVE_PATH);
		this.ddFileName.setItems(FXCollections.observableList(SUPPORTED_IMAGE_TYPES));
		this.ddFileName.setValue(DEFAULT_FILE_EXT);
		
		
	}
	
	public void setApplication(Main application){
		this.app = application;
	}
	
	public void handleBtnSavePathAction(ActionEvent event){
		if (this.app != null){
			final DirectoryChooser dirChooser = new DirectoryChooser();
			dirChooser.setInitialDirectory(new File(DEFAULT_SAVE_PATH));
            final File selectedDir = dirChooser.showDialog(this.app.getStage());
            
            if(selectedDir != null){
    			this.tfSavePath.setText(selectedDir.getPath());
            }
        }
	}
	
	public void handleCbFileNameAction(ActionEvent event){
		if(this.app != null){
			this.tfFileName.setEditable(this.cbFileName.isSelected());
		}
	}
	
	public void handleCbTextAction(ActionEvent event){
		if(this.app != null){
			this.tfText.setEditable(this.cbText.isSelected());
		}
	}
	
	public void handleTfSizeChange(Event event){
		
		if(cbFileName.isSelected()){
			return;
		}
		final String filename = this.tfWidth.getText() + "x" + this.tfHeight.getText() + "px";
		tfFileName.setText(filename);
	}
	
	public void handleExitApp(Event event){
		this.app.getStage().close();
	}
	
	public void handleBtnCreateImageAction(ActionEvent event){
		final StringBuilder errorMsg = new StringBuilder();
		if(tfHeight.getLength() == 0){
			errorMsg.append("Please enter a value for height!");
		}
		if(tfWidth.getLength() == 0){
			if(errorMsg.length() != 0){
				errorMsg.append("\n");
			}
			errorMsg.append("Please enter a value for Width!");
		}
		if(tfFileName.getLength() == 0){
			if(errorMsg.length() != 0){
				errorMsg.append("\n");
			}
			errorMsg.append("Please enter a file name");
		}
		if(errorMsg.length() != 0){
			lblErrorMessage.setText(errorMsg.toString());
			return;
		}
		
		final int width = Integer.parseInt(tfWidth.getText());
		final int height = Integer.parseInt(tfHeight.getText());
		final String text = tfText.getText();
		final String fileName = tfFileName.getText();
		final String extention = ddFileName.getValue();
		final String path = tfSavePath.getText();
		final Color backgroundColor = cpBackgroundColor.getValue();
		final Color textColor = cpTextColor.getValue();
		
		final java.awt.Color awtBackgroundColor = new java.awt.Color((float)backgroundColor.getRed(), (float)backgroundColor.getGreen(), (float)backgroundColor.getBlue());
		final java.awt.Color awtTextColor = new java.awt.Color((float)textColor.getRed(), (float)textColor.getGreen(), (float)textColor.getBlue());
		
		try {
			TestImageFactory.createImage(fileName, extention, path, text, width, height, awtBackgroundColor, awtTextColor);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
