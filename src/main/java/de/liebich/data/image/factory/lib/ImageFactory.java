package de.liebich.data.image.factory.lib;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Class which provides different static methods to create an image.
 * 
 * @author liebichs
 * 
 */
public class ImageFactory {

	private static final String TEXT_FONT = "Courier New";

	/**
	 * Static method which creates from the given configuration an image an
	 * saves it on defined location. The text is displayed as courier new as
	 * default.
	 * 
	 * @param fileName
	 *            - Name of the file to save
	 * @param extension
	 *            - File extension
	 * @param path
	 *            - Location where to save the image
	 * @param text
	 *            - Text to display on the image
	 * @param width
	 *            - Image width
	 * @param height
	 *            - Image height
	 * @param backgroundColor
	 *            - Background color of the image
	 * @param textColor
	 *            - Color of displayed text
	 * @throws IOException
	 *             - If an error occurs during writing.
	 */
	public static void createImage(final String fileName, final String extension, final String path, final String text,
	        final int width, final int height, final Color backgroundColor, final Color textColor) throws IOException {

		final File fileLocation = new File(path + (path.charAt(path.lastIndexOf(File.separator))) + fileName + "."
		        + extension);

		final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g2d = bufferedImage.createGraphics();
		g2d.setColor(backgroundColor);
		g2d.fillRect(0, 0, width, height);

		int textSize = height;
		g2d.setFont(new Font(TEXT_FONT, Font.BOLD, textSize));
		FontMetrics fm = g2d.getFontMetrics();
		while (fm.stringWidth(text) + 20 > width) {
			textSize -= 2;
			g2d.setFont(new Font(TEXT_FONT, Font.BOLD, textSize));
			fm = g2d.getFontMetrics();
		}

		g2d.setColor(textColor);
		g2d.drawString(text, 10, (height / 2) + (fm.getHeight() / 4));

		g2d.dispose();

		ImageIO.write(bufferedImage, extension, fileLocation);
	}

}
