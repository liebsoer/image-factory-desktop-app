package de.liebich.data.image.factory.app;
	
import java.io.InputStream;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;


public class Main extends Application {
	private Stage stage;
	
	@Override
	public void start(Stage primaryStage) {
		this.stage = primaryStage;
		stage.setTitle("Test Image Factory");
		
		try {
			
			TestImageFactoryControler login = (TestImageFactoryControler) replaceSceneContent(UIUtils.getFXMLPath("TestImageFactory"));
            login.setApplication(this);

            this.stage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private Initializable replaceSceneContent(String fxml) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = Main.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Main.class.getResource(fxml));
        StackPane page;
        try {
            page = (StackPane) loader.load(in);
        } finally {
            in.close();
        } 
        Scene scene = new Scene(page);
        scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
        stage.setScene(scene);
        stage.sizeToScene();
        return (Initializable) loader.getController();
    }
	
	public static void main(String[] args) {
		launch(args);
	}
	
	protected Stage getStage(){
		return this.stage;
	}
}
