package de.liebich.data.image.factory.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class PropertiesUtils{
	static final String RESSOURCES_DEFAULT_PATH = "ressources/";
	
	public static Properties getProperties(final URI path){
		final Properties prop = new Properties();
        try {
        	InputStream is = new FileInputStream(new File(path));
        	prop.load(is);
        } catch (IOException e) {
	        e.printStackTrace();
        }
		return prop;
	}

	public static Properties getProperties(final String filename){
		return getProperties(filename, RESSOURCES_DEFAULT_PATH);
	}

	public static Properties getProperties(final String filename, final String path){
        return getProperties(new File(path + (path.charAt(path.length() - 1) != File.separatorChar || path.charAt(path.length() - 1) != '/' ? "/" : "") + filename).toURI());
	}
	
	public static Properties getProperties(final URL path){
		try {
	        return getProperties(path.toURI());
        } catch (URISyntaxException e) {
	        e.printStackTrace();
	        return new Properties();
        }
	}
	
	
}
