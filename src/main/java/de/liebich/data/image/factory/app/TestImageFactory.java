package de.liebich.data.image.factory.app;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class TestImageFactory {

	private static final String TEXT_FONT = "Courier New";
	
	public static void createImage(final String fileName, final String extention, final String path, final String text, final int width, final int height, final Color backgroundColor, final Color textColor) throws IOException{
		final File f = new File(path + (path.charAt(path.lastIndexOf(File.separator))) + fileName + "." + extention);
		
	    BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	    Graphics2D g2d = bufferedImage.createGraphics();
	    g2d.setColor(backgroundColor);
	    g2d.fillRect(0, 0, width, height);
	    
	    int textSize = height;
	    g2d.setFont(new Font(TEXT_FONT, Font.BOLD, textSize));
	    FontMetrics fm = g2d.getFontMetrics();
	    while(fm.stringWidth(text) + 20 > width){
	    	textSize -= 2;
	    	g2d.setFont(new Font(TEXT_FONT, Font.BOLD, textSize));
	    	fm = g2d.getFontMetrics();
	    }
	    
	    g2d.setColor(textColor);
	    g2d.drawString(text, 10, (height / 2) + (fm.getHeight() / 4));

	    g2d.dispose();

	    ImageIO.write(bufferedImage, extention, f);
	}
	
}
