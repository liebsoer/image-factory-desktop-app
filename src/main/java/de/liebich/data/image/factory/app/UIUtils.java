package de.liebich.data.image.factory.app;

public class UIUtils {
	private static String BASEPATH = "/ressources/ui/";
	
	public static String getFXMLPath(final String name){
		return BASEPATH + name + ".fxml";
	}
}
